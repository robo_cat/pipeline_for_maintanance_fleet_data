import pandas as pd
from quality_checks import is_valid_email

def merge_dataframes(maintenance_df, fleet_df):
    """
    Merges two data frame on the `truck_id` column.
    
    Parameters:
    - maintenance_df (pandas.DataFrame): dataframe for the maintanence data
    - fleet_df (pandas.DataFrame): dataframe for the fleet data

    Returns:
    - pandas.DataFrame: Returns merged dataframe
    """

    # merge given two data set based on 'truck_id' that both data sets include
    merged_df = pd.merge(maintenance_df, fleet_df, on='truck_id', how='inner')
    return merged_df

def standardize_service_type(df):
    """
    Standardize the service_type column by converting to lowercase and stripping whitespaces.
    
    Parameters:
    - df (pandas.DataFrame): given data set to standardize 'service_type' column
    """
        
    # Converting service_type column to lowercase and stripping whitespaces.
    df['service_type'] = df['service_type'].str.lower().str.strip()

def one_hot_encode_service_type(df):
    """
    Performs One-Hot encoding on the service_type column.
    
    Parameters:
    - df (pandas.DataFrame): given data set

    Returns:
    - pandas.DataFrame: Returns new dataframe that One-Hot encoding performed on the service_type column
    """

    # Performs One-Hot encoding on the standardized service_type column. 
    # This will be add three extra columns to our dataset in place of out service_type.
    df = pd.get_dummies(df, columns=['service_type'])
    return df

def validate_technician_email(maintenance_df):
    """
    Adds a valid_email column (boolean) indicating the validity of technician_email based on our data quality criteria.
    
    Parameters:
    - df (pandas.DataFrame): given data set
    """

    # A function that adds a valid_email column (boolean) indicating the validity of technician_email based on our data quality criteria.
    # Create an empty list to assign new created column for the data frame
    mail_validity_list = []

    # for loop to check if the mails are valid
    for mail in maintenance_df['technician_email']:
        # call is_valid_email for each mail and assign the boolaean result to the list
        mail_validity_list.append(is_valid_email(mail, "maintanence"))
    
    # Inserting the new 'valid_email' column after the 'technician_email' column
    maintenance_df.insert(maintenance_df.columns.get_loc('technician_email') + 1, 'valid_email', mail_validity_list)

def format_dates(fleet_df, maintenance_df):
    """
    Ensures our purchase_date and maintenance_date are in our desired format (day-month-year).
    Any failures shouldn't be corrected but instead set to NaT (errors='coerce')
    
    Parameters:
    - maintenance_df (pandas.DataFrame): dataframe for the maintanence data
    - fleet_df (pandas.DataFrame): dataframe for the fleet data
    """
    
    # Convert all values to the datetime object
    fleet_df['purchase_date'] = pd.to_datetime(fleet_df['purchase_date'], errors='coerce', format="%d-%m-%Y")
    # Set the format
    fleet_df['purchase_date'] = fleet_df['purchase_date'].dt.strftime("%d-%m-%Y")

    # Convert all values to the datetime object
    maintenance_df['maintenance_date'] = pd.to_datetime(maintenance_df['maintenance_date'], errors='coerce', format="%d-%m-%Y")
    # Set the format
    maintenance_df['maintenance_date'] = maintenance_df['maintenance_date'].dt.strftime("%d-%m-%Y")


