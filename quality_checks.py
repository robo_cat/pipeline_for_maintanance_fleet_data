import logging
import pandas as pd
import re
from utils import setup_logger


# Set up the logger to keep track of data quality under log folder
data_quality_logger = setup_logger('data_quality', 'data_quality.log', logging.INFO)

def get_missing_truck_ids(maintenance_df, fleet_df):
    """
    Finds missing 'truck_id' values from the fleet data.
    
    Parameters:
    - maintenance_df (pandas.DataFrame): DataFrame with unprocessed maintanence records data
    - fleet_df (pandas.DataFrame): DataFrame with unprocessed fleet information data

    Returns:
    - list: Returns a list of unique truck_id that are present in the maintenance data and not the fleet data.
    """

    # Get unique truck_ids from maintenance data
    maintenance_truck_ids = set(maintenance_df['truck_id'])
    # Get unique truck_ids from fleet data
    fleet_truck_ids = set(fleet_df['truck_id'])
    # Find missing truck_ids in fleet data
    missing_truck_ids = maintenance_truck_ids - fleet_truck_ids

    if len(missing_truck_ids) > 0:
        data_quality_logger.info(f"Truck IDs missing from the fleet dataset: {', '.join(missing_truck_ids)}")
    else:
        data_quality_logger.info(f"Truck IDs missing from the fleet dataset: NONE")

    return list(missing_truck_ids)


def find_missing_values(df, file_name):

    """
    Finds missing values from the given data set.
    
    Parameters:
    - df (pandas.DataFrame): DataFrame with unprocessed data
    - file_name (str): file name to add logs to keep it in track

    Returns:
    - pandas.Series: Returns a series with missing counts per column
    """
   
    # Check if the given data set has any null values
    missing_data_counts = df.isnull().sum()

    missing_data_counts = missing_data_counts[missing_data_counts > 0]

    # If data set has any missing values log it to data_quality file
    if missing_data_counts.any():
        data_quality_logger.info(f"Missing Data in {file_name}:")
        # Log for the each missing column with name and count
        for column, count in missing_data_counts.items():
            if count > 0:
                data_quality_logger.info(f"  {column}: {count} missing values")

    return missing_data_counts
    
def find_duplicated_values(df, file_name):

    """
    Finds duplicated values from the given data set.
    
    Parameters:
    - df (pandas.DataFrame): DataFrame with unprocessed data
    - file_name (str): file name to add logs to keep it in track

    Returns:
    - int: Returns a number of duplicated entries
    """

    # Check if the given data set has any duplicated values
    duplicate_records = df.duplicated().sum()

    # Log any duplicate records with file name and count of duplicated entries
    if duplicate_records > 0:
        data_quality_logger.info(f"Duplicate records found for {file_name} data: {duplicate_records}")


    return duplicate_records


def is_valid_email(email, file_name):

    """
    Validates given email addresses.
    
    Parameters:
    - email (str) : the email str to check
    - file_name (str): The name of the file or dataset.
    
    Returns:
    - boolean : Return true or false based on the given email matches to the email format
    """
    
    # Regular expression pattern for email validation
    email_format = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$'

    # Check if the email is empty or null
    if pd.isna(email) or email == "":
        data_quality_logger.info(f"Invalid email (null or empty) in {file_name} data.")
        return False

    # Check if the email matches the format
    if re.match(email_format, email):
        return True
    else:
        data_quality_logger.info(f"Invalid email in {file_name} data: {email}")
        return False