import json
import pandas as pd
from quality_checks import find_duplicated_values, find_missing_values, get_missing_truck_ids
from processing import format_dates, merge_dataframes, validate_technician_email, standardize_service_type, one_hot_encode_service_type
from utils import setup_logger, get_file_path
import argparse


def main(data_path, output_path):
    """
    Loads data from a json file, applies cleaning operations and saves the cleaned data to a new CSV file.

    Parameters:
    - data_path (str): The file path to the input.
    - output_path (str): The file path where the cleaned data should be saved.
    """

    # Confirmation message to indicate the process is started
    print(f"Data pipeline is started {data_path} ...")

    # 1 #
    # set the application error logger
    app_errors_logger = setup_logger('application_errors', 'application_errors.log')

    # Load configuration file from given JSON
    try:
        with open('data_sources.json', 'r') as data_source_file:
            data_sources = json.load(data_source_file)  
    except Exception as e:
        app_errors_logger.error(f"An error occurred while reading file 'data_sources.json': {str(e)}")
        raise SystemExit("Error loading configuration file, operation is terminated!")


    # Initialize empty DataFrames
    maintenance_df = pd.DataFrame()
    fleet_df = pd.DataFrame()

    # 2 #
    # Load data to the data frame
    try:
        for source in data_sources:
            if source['type'] == 'maintenance':
                # Obtain the correct path for the different os system
                maintanence_data_path = get_file_path("data", source['path'])
                maintenance_df = pd.read_csv(maintanence_data_path) # Load maintenance data
            elif source['type'] == 'fleet':
                # Obtain the correct path for the different os system
                fleet_data_path = get_file_path("data", source['path'])
                fleet_df = pd.read_csv(fleet_data_path) # Load fleet data
            else:
                raise FileNotFoundError(f"{source['type']} data file not found")
        
    except FileNotFoundError as e:
        app_errors_logger.error(f"An error occurred while reading file '{source['path']}': {str(e)}")
        raise SystemExit(f"Error while reading file '{source['path']}, operation is terminated!")

    except Exception as e:
        app_errors_logger.error(f"An unexpected error occurred: {str(e)}")
        raise SystemExit(f"Error occured: {str(e)}, operation is terminated!")

    # Check the quality of the data sets

    # 3 # 
    # Analyse duplicated data and if exist remove
    if (find_duplicated_values(fleet_df, "fleet") > 0):
        fleet_df.drop_duplicates(inplace = True)
        
    if (find_duplicated_values(maintenance_df, "maintanence") > 0):
        maintenance_df.drop_duplicates(inplace = True)

    # 4 # 
    # Analyse missings values
    find_missing_values(fleet_df, "fleet")
    find_missing_values(maintenance_df, "maintanence")

    # 5 #
    # Identify and logs truck_id present in the maintenance dataset but missing from the fleet dataset
    get_missing_truck_ids(maintenance_df, fleet_df)

    # 6 #
    # Convert date columns to a consistent datetime format.
    format_dates(fleet_df, maintenance_df)

    # 7 #
    # Standardizes the service_type column by converting it to lowercase and stripping any leading or trailing whitespace.
    standardize_service_type(maintenance_df)

    # 8 #
    # Applies one-hot encoding to the service_type column to facilitate analysis.
    maintenance_df = one_hot_encode_service_type(maintenance_df)

    # 9 #
    # Validates the format of technician email addresses, adding a boolean email_valid column to indicate validity.
    validate_technician_email(maintenance_df)

    # 10 #
    # Merges the fleet and maintenance datasets on truck_id
    merged_dataframes = merge_dataframes(maintenance_df, fleet_df)

    # 11 #
    # Saved the merged dataframe to fleet_maintenance_MERGED.csv
    merged_dataframe_saved_path = get_file_path("processed_data", "fleet_maintenance_MERGED.csv")
    merged_dataframes.to_csv(merged_dataframe_saved_path, index=False, mode='w')

    # Confirmation message to indicate successful completion of the process.
    print(f"Cleaned data saved to {output_path}")

if __name__ == "__main__":
    # Set up an argument parser to allow command-line parameters for the script.
    parser = argparse.ArgumentParser(description='Run data cleaning operations on a CSV dataset.')
    
    # Define expected command-line arguments: input data file, configuration file, and output file path.
    parser.add_argument('data_path', help='Path to the input json file containing the path for the data to be cleaned.')
    parser.add_argument('output_path', help='Path where the cleaned data CSV file should be saved.')
    
    # Parse the arguments provided by the user.
    args = parser.parse_args()

    # Execute the main function using the provided arguments.
    main(args.data_path, args.output_path)






