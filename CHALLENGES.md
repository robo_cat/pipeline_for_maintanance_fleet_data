# Assignment Reflection

## Introduction
In this markdown file, I will reflect on my experience with the assignment, detailing the challenges faced and my approach to problem-solving.

## Experience
Overall, the assignment provided a valuable learning opportunity to apply various data processing and quality checks techniques using Python. I found the tasks to be engaging and relevant to real-world scenarios, which helped reinforce my understanding of data handling best practices.
I could experiment on 
1) Data Ingestion to read the provided datasets into your Python environment for processing.
2) Data Quality Assessment to conduct thorough quality checks on the data, identifying and logging any discrepancies, missing values, or anomalies.
3) Data Cleaning and Transformation to prepare the data for analysis by cleaning and transforming it as necessary, including standardizing formats, handling missing values, and encoding categorical variables.
4) Data Integration to merge the maintenance and fleet datasets into a single, unified dataset for analysis, using the truck_id as the key.
5) Documentation and Logging to ensure all significant events, findings, and transformations are well-documented and logged for auditability and reproducibility.

## Challenges Faced
One of the main challenges I encountered was managing circular imports between modules, particularly when importing functions from the `quality_checks` module in the `pipeline` module. This issue required careful restructuring of the code and module dependencies to resolve.

Another challenge is the formatting date in `processing`. Pandas to_datetime function with given format did not work for our dataset because not every entry is date in the data file. After converting the values to datetime objects, then it could be formatted with the specified format ("%d-%m-%Y"). 

Another challenge is the given optional prosessing step "Calculate and add a column maintenance_cost_per_km, derived by dividing the maintenance cost by the distance traveled since the last service. Use the formula: maintenance cost / (current mileage - mileage at last service)" here calculating the milage at last service needs comprehensive data operation based on the maintanence date and I could not find the correct algorithm for it.

Additionally, handling null values and exceptions gracefully posed some difficulties, especially when dealing with data validation and formatting functions. Ensuring effective logging was crucial in addressing these challenges.

## Problem-Solving Approach
To overcome the challenges faced, I adopted a systematic problem-solving approach:

1. **Identify the Issue**: Thoroughly analyze the error messages and code structure to pinpoint the root cause of the problem.
2. **Restructure Code**: Modify the code structure, module imports, or function implementations as needed to resolve circular imports and address null value handling.
3. **Implement Error Handling**: Integrate try-except blocks and robust error handling mechanisms to gracefully manage exceptions and unexpected scenarios.
4. **Implement Debug Logging**: Integrate info logging mechanisms to gracefully record and follow wrong behavior and unexpected scenarios.
5. **Test and Iterate**: Test the revised code extensively, ensuring that it behaves as expected under various conditions. Iterate on the solution as necessary until the desired functionality is achieved.
Steps:
 * Change the data file names and observe the behavior
 * Copy the data set not to lose original then play with data if results are correct
 * Check the merged dataframe if it matches with the requirements

## Additions
To improve the pipeline integrate try-except blocks to the `quality_checks` and `processing` then if something is not detect during the checks or process system will not create fleet_maintenance_MERGED.csv to avoid wrong data set.

## Reflections
Overall, the assignment provided valuable insights into the complexities of real-world data processing tasks and the importance of robust error handling and logging practices. By overcoming challenges and applying effective problem-solving strategies, I was able to enhance my skills in Python programming and data processing techniques.

Moving forward, I aim to further refine my understanding of data quality assurance methodologies and continue exploring advanced techniques for data manipulation and analysis.
