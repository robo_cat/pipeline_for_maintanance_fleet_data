# Data Analysis On Maintanence And Fleet Of Trucks

This project purpose is a comprehensive data analysis pipeline focused on maintenance records for a fleet of trucks. The primary goal is to integrate maintenance data with fleet information to offer in-depth insights into the maintenance history and each truck in the fleet.

## Setup and Installation

* Clone the repository
* Then, you can run your code with python3 pipeline.py data_sources.json fleet_maintenance_MERGED.csv   
where   
**pipeline.py** is main class which holds automated functions
**data_sources.json** is the configuration file that keeps paths for data (under data folder)
**fleet_maintenance_MERGED.csv** is the saved file after cleaning operations (under processed_data after pipeline run successfully)

**Version: Python 3.11.8**

## Dependencies

The project has the following dependencies:

* You might need to update or install matplotlib library if it is needed run below code
1) pip3 uninstall matplotlib (if it doesn`t exist skip this step)
2) pip3 install matplotlib
* You might need to update or install scipy library if it is needed run below code
1) pip3 uninstall scipy (if it doesn`t exist skip this step)
2) pip3 install scipy


## Project Structure

1) **data_sources.json:** This file serves as the configuration file, containing the paths to the datasets used in the project.
2) **data:** The datasets are stored in the data folder to maintain organization and ease of access.
3) **logs:**
- **application_errors:** This directory contains logs related to application errors.
- **data_quality:** Logs for the data quality checks are stored in this directory.
4) **utils.py:** This file houses general-purpose functions that are utilized across different modules within the project.
5) **quality_checks.py:** This module is dedicated to checking the quality of the data and logging any relevant information regarding data quality.
6) **processing.py:** Here, the data transformation tasks are carried out according to the project requirements.
7) **pipeline.py:** Serving as the main class to initiate the data processing pipeline, this file orchestrates the entire data processing workflow.
8) **processed_data:** Upon completion of the data pipeline, the cleaned and merged dataset is saved under the processed_data folder for further analysis or usage.

## Authors

* Belgin Vatansever

## License

* This is an assignment for the Data Transformation and Cleaning module course. 

Copyright (c) 2024-2025 Scania Academy.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Acknowledgments

Thanks to Nicholas Lennox for the great tutorial!