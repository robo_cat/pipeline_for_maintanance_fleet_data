import logging
import os 


def get_file_path(folder_name, filename):
    """
    Finds the corresponding path for different os system.
    
    Parameters:
    - folder_name (str): folder name for the file
    - filename (str): file name for the saved or existing file

    Returns:
    - str: Path string for the corresponding os system
    """

    # Create the full path of given file
    return os.path.join( os.sep, os.getcwd(), folder_name, filename)

def setup_logger(name, log_file, level = logging.ERROR, log_format = '%(asctime)s - %(levelname)s - %(message)s'):
    """
    Create logger for the project.
    setup_logger('data_quality', 'data_quality.log', logging.INFO)
    Parameters:
    - name (str): name of the logging
    - log_file (str): log file name
    - level (int): level of the logging default ERROR
    - log_format (str): log format

    Returns:
    - Logger: Logger object to setup necessary logs
    """

    log_file_path = get_file_path("logs", log_file)
    handler = logging.FileHandler(log_file_path)     
    formatter = logging.Formatter(log_format)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger


